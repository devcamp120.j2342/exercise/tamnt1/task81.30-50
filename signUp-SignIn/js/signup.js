$(document).ready(function() {
    //Sự kiện bấm nút sign up
    $("#submit").on("click", function() {
        var username = $("#inputUsername").val().trim();
        var email = $("#inputEmail").val().trim();
        var password = $("#inputPassword").val().trim();
        var confirmPassword = $("#inputConfirmPassword").val().trim();

        if (validateForm(username, email, password, confirmPassword)) {
            signUpForm(username, email, password);
        }
    });

    function signUpForm(username, email, password) {
        var signUpUrl = "http://localhost:8080/signup";

        var vSignUpData = {
            username: username,
            email: email,
            password: password
        };

        $.ajax({
            url: signUpUrl,
            type: "POST",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: JSON.stringify(vSignUpData),
            success: function(responseObject) {
                responseHandler(responseObject);
            },
            error: function(xhr) {
                // Lấy error message
                var errorMessage = xhr.responseJSON.message;
                showError(errorMessage);
            }
        });
    }

    //Xử lý object trả về khi sign up thành công
    function responseHandler(data) {
        alert("Sign-up successful!");
        window.location.href = "login.html";
    }

    //Hiển thị lỗi lên form
    function showError(message) {
        var errorElement = $("#error");

        errorElement.html(message);
        errorElement.removeClass("d-none");
        errorElement.addClass("d-block");
    }

    //Validate dữ liệu từ form
    function validateForm(username, email, password, confirmPassword) {
        if (username === "") {
            alert("Username không phù hợp");
            return false;
        }

        if (email === "") {
            alert("Email không phù hợp");
            return false;
        }

        if (password === "") {
            alert("Password không phù hợp");
            return false;
        }

        if (confirmPassword === "") {
            alert("Confirm Password không phù hợp");
            return false;
        }

        if (password !== confirmPassword) {
            alert("Confirm Password không trùng khớp");
            return false;
        }

        return true;
    }
});
